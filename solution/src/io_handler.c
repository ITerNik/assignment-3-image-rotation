#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "io_handler.h"

#define INIT_FILE_ERROR_MES "Failed to initialize file pipe"

const char* const FORMAT_ERROR_TEXT_REPR[] =  {[FORMAT_OK] = "Format is correct",
    [INCORRECT_ANGLE_TEXT] = "Got invalid angle",
    [INCORRECT_FORMAT] = "Format is image-transformer",
    [ANGLE_NOT_AVAILABLE] = "Valid angles are: -270, -180, -90, 0, 90, 180, 270"
};

const char* const FILE_ERROR_TEXT_REPR[] = {[FILE_OK] = "Files opened successfully",
    [ERROR_OPEN_INPUT_FILE] = "Error occurred while trying to open input file",
    [ERROR_OPEN_OUTPUT_FILE] = "Error occurred while trying to open output file",
    [ERROR_FILE_CLOSE] = "Error occurred while closing file"
};


struct pipe_file {
    FILE* in;
    FILE* out;
    enum file_status status;
};

enum format_status check_angle(char* angle) {
    if (strcmp(angle, "0") == 0) return FORMAT_OK;
    int64_t numeric_angle = atoi(angle);
    if (!numeric_angle) return INCORRECT_ANGLE_TEXT;
    if (numeric_angle >= -270 &&
        numeric_angle <= 270 &&
        (numeric_angle % 90 == 0)) {
            return FORMAT_OK;
    } else return ANGLE_NOT_AVAILABLE;
}

enum format_status check_arguments(int32_t argc) {
    if (argc == 4) return FORMAT_OK;
    return INCORRECT_FORMAT;
}

enum format_status check_format(char* angle, int32_t argc) {
    enum format_status st = check_arguments(argc);
    if (st != FORMAT_OK) return st;
    st = check_angle(angle);
    if (st != FORMAT_OK) return st;
    return FORMAT_OK;
}

struct pipe_file* invalid_pipe_file(enum file_status st) {
    struct pipe_file* pf = malloc(sizeof(struct pipe_file));
    *pf = (struct pipe_file){NULL, NULL, st};
    if (!pf) {
        fprintf(stderr, INIT_FILE_ERROR_MES);
        return NULL;
    }
    return pf;
}

struct pipe_file* open_pipe_file(char* in_name, char* out_name) {
    FILE* in = fopen(in_name, "r");
    if (!in) return invalid_pipe_file(ERROR_OPEN_INPUT_FILE);
    FILE* out = fopen(out_name, "w");
    if (!out) {
        fclose(in);
        return invalid_pipe_file(ERROR_OPEN_OUTPUT_FILE);
    }

    struct pipe_file* pf = malloc(sizeof (struct pipe_file));
    if (!pf) {
        fprintf(stderr, INIT_FILE_ERROR_MES);
        return NULL;
    }
    *pf = (struct pipe_file){in, out, FILE_OK};
    return pf;
}

#define PIPE_GETTER(type, name) \
type get_pipe_file_##name(const struct pipe_file* pf) { \
    return pf->name; \
}

PIPE_GETTER(enum file_status, status)
PIPE_GETTER(FILE*, in)
PIPE_GETTER(FILE*, out)

void free_pipe_file(struct pipe_file** pipe) {
    if((*pipe)->status == FILE_OK) {
        fclose((*pipe)->in);
        fclose((*pipe)->out);
    }
    free(*pipe);
}

