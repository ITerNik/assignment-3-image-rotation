#include <stdio.h>
#include <stdlib.h>

#include "image.h"

#define INIT_IMG_ERROR_MES "Error occurred while trying to initialize image"
#define INIT_PIXEL_ERROR_MES "Error occurred while trying to initialize pixel data"

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

const size_t SIZEOF_PIXEL = sizeof(struct pixel);

const size_t SIZEOF_IMAGE = sizeof(struct image);

struct pixel* get_pixel_at(struct pixel* data, size_t index) {
    return data + index;
}

void set_image_pixel_from(struct image* dest, size_t to, const struct image* src, size_t from) {
    dest->data[to] = src->data[from];
}

#define IMAGE_GETTER(prop, type) \
type get_image_##prop(const struct image *img) { \
    return img->prop; \
}

#define IMAGE_SETTER(prop, type) \
void set_image_##prop(struct image *img, type val) { \
    img->prop = val; \
}

IMAGE_GETTER(height, uint64_t)
IMAGE_GETTER(width, uint64_t)
IMAGE_GETTER(data, struct pixel*)

IMAGE_SETTER(height, uint64_t)
IMAGE_SETTER(width, uint64_t)
IMAGE_SETTER(data, struct pixel*)

struct pixel* init_pixel_data(size_t size){
    struct pixel* data = malloc(size * sizeof(struct pixel));
    if (data) {
        return data;
    } else {
        fprintf(stderr, "%s\n", INIT_PIXEL_ERROR_MES);
        return NULL;
    }
}

struct image* init_image(void) {
    struct image* new_image = malloc(sizeof(struct image));
    if (!new_image) {
        fprintf(stderr, "%s\n", INIT_IMG_ERROR_MES);
        return NULL;
    }
    new_image->data = NULL;
    new_image->width = 0;
    new_image->height = 0;
    return new_image;
}

struct image* construct_image(uint64_t width, uint64_t height) {
    struct image* new_image = malloc(SIZEOF_IMAGE);
    if (!new_image) {
        fprintf(stderr, "%s\n", INIT_IMG_ERROR_MES);
        return NULL;
    }
    new_image->data = init_pixel_data(width * height);
    if (!new_image->data) {
            free(new_image);
            return NULL;
    }
    new_image->width = width;
    new_image->height = height;
    return new_image;
}

struct image* copy_image(const struct image* img) {
    struct image* new_image = construct_image(get_image_width(img), get_image_height(img));
    if (!new_image) return NULL;
    for (size_t i = 0; i < get_image_height(img) * get_image_width(img); ++i) {
        new_image->data[i] = img->data[i];
    }
    return new_image;
}

void deinit_image(struct image** img) {
    free((*img)->data);
    free(*img);
}
