#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "transformation.h"

#define ROTATE_ERROR_MES "Failed to rotate image"

typedef struct image* (*rotating_function) (struct image const*);

struct image* rotate90(struct image const* img) {
    struct image* rotated = construct_image(get_image_height(img), get_image_width(img));
    if (!rotated) return NULL;
    for (size_t r = 0; r < get_image_height(img); ++r) {
       for (size_t c = 0; c < get_image_width(img); ++c) {
            set_image_pixel_from(rotated, (get_image_height(rotated) - c - 1) * get_image_width(rotated) + r,
                                  img, r * get_image_width(img) + c);
       }
    }
    return rotated;
}

struct image* rotate180(struct image const* img) {
    struct image* rotated = construct_image(get_image_width(img), get_image_height(img));
    if (!rotated) return NULL;
    for (size_t r = 0; r < get_image_height(img); ++r) {
       for (size_t c = 0; c < get_image_width(img); ++c) {
            set_image_pixel_from(rotated, (get_image_height(rotated) - r - 1) * get_image_width(rotated) + get_image_width(rotated) - c - 1,
                                   img, r * get_image_width(img) + c);
       }
    }
    return rotated;
}

struct image* rotate270(struct image const* img) {
    struct image* rotated = construct_image(get_image_height(img), get_image_width(img));
    if (!rotated) return NULL;
    for (size_t r = 0; r < get_image_height(img); ++r) {
       for (size_t c = 0; c < get_image_width(img); ++c) {
           set_image_pixel_from(rotated, c  * get_image_width(rotated) + get_image_width(rotated) - r - 1,
                                img, r * get_image_width(img) + c);
       }
    }
    return rotated;
}

rotating_function ROTATING[] = {[0] = &copy_image, [90] = &rotate90,
    [180] = &rotate180, [270] = &rotate270
};

struct image* rotation(const struct image* img, int64_t angle) {
    int64_t tranformed_angle = angle % 360;
    tranformed_angle = tranformed_angle < 0 ? tranformed_angle + 360 : tranformed_angle;

    return ROTATING[tranformed_angle] (img); // Assuming angle format is checked already

    fprintf(stderr, "%s\n",  ROTATE_ERROR_MES);
    return NULL;
}
