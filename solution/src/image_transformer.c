#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp_handler.h"
#include "io_handler.h"
#include "transformation.h"


int32_t main(int32_t argc, char** argv) {
    enum format_status format_st = check_format(argv[3], argc);
    if (format_st != FORMAT_OK) {
        fprintf(stderr, "%s\n", FORMAT_ERROR_TEXT_REPR[format_st]);
        return (int32_t) format_st;
    }
    struct pipe_file* pipe = open_pipe_file(argv[1], argv[2]);
    enum file_status file_st = get_pipe_file_status(pipe);

    if (file_st != FILE_OK) {
        perror(FILE_ERROR_TEXT_REPR[file_st]);
        free_pipe_file(&pipe);
        return file_st;
    }

    struct image* img = init_image();
    if (!img) {
        free_pipe_file(&pipe);
        return -1;
    }

    enum read_status st_read = from_bmp(get_pipe_file_in(pipe), img);
    if (st_read != READ_OK) {
        free_pipe_file(&pipe);
        deinit_image(&img);
        fprintf(stderr, "%s\n", READ_STATUS_TEXT_REPR[st_read]);
        return (int32_t) st_read;
    }

    struct image* trans = rotation(img, atoi(argv[3]));
    if (!trans) {
        free_pipe_file(&pipe);
        deinit_image(&img);
        return -1;
    }
    enum write_status st_write = to_bmp(get_pipe_file_out(pipe), trans);
    if (st_write != WRITE_OK) {
        free_pipe_file(&pipe);
        deinit_image(&img);
        deinit_image(&trans);
        fprintf(stderr, "%s\n", WRITE_STATUS_TEXT_REPR[st_write]);
        return (int32_t) st_write;
    }
    free_pipe_file(&pipe);
    deinit_image(&trans);
    deinit_image(&img);
    return 0;
}
