#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp_handler.h"

#define BMP_TYPE_ID 0x4D42
#define HEADER_SIZE 40
#define COLOR_PLANES 1
#define BI_RGB 0
#define DEFAULT_OR_IGNORE 0

 const char* const READ_STATUS_TEXT_REPR[] = {
    [READ_OK] = "Read successfully", [READ_INVALID_SIGNATURE] = "Wrong file format: .bmp required",
    [READ_INVALID_BITS] = "Can't read invalid pixel data", [READ_INVALID_HEADER] = "Can't read invalid header",
    [READ_ERROR] = "Error occurred while trying to read the file"
};

 const char* const WRITE_STATUS_TEXT_REPR[] = {
    [WRITE_OK] = "Wrote successfully", [WRITE_INVALID_HEADER] = "Can't write invalid header",
    [WRITE_ERROR] = "Error occurred while trying to write into the file", [WRITE_INVALID_BITS] = "Can't write invalid pixel data",
    [WRITE_INVALID_SOURCE_IMAGE] = "Can't convert invalid source image"
};

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

union bmp_status {
    enum read_status read_status;
    enum write_status write_status;
};

#define BMP_WITH_STATUS(name, type) \
struct bmp_##name##_with_status { \
    type* name; \
    union bmp_status status; \
}; \
\
struct bmp_##name##_with_status invalid_read_##name(enum read_status status) { \
    struct bmp_##name##_with_status instance = {NULL}; \
    instance.status.read_status = status; \
    return instance; \
} \
\
struct bmp_##name##_with_status invalid_write_##name(enum write_status status) { \
    struct bmp_##name##_with_status instance = {NULL}; \
    instance.status.write_status = status; \
    return instance; \
} \
 \
 \
void free_bmp_##name##_with_status(struct bmp_##name##_with_status instance) { \
    free(instance.name); \
}


BMP_WITH_STATUS(header, struct bmp_header)
BMP_WITH_STATUS(data, struct pixel)

struct bmp_header_with_status some_header(struct bmp_header* header) {
    struct bmp_header_with_status instance = { header , .status = {READ_OK}};
    return instance;
}

struct bmp_data_with_status some_data(struct pixel* data) {
    struct bmp_data_with_status instance = { data , .status = {READ_OK}};
    return instance;
}

struct bmp_header* get_default_header(void) {
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    if (!header) return NULL;
    header->bfType = BMP_TYPE_ID;
    header->bfReserved = DEFAULT_OR_IGNORE;
    header->biSize = HEADER_SIZE;
    header->biPlanes = COLOR_PLANES;
    header->biCompression = BI_RGB;
    header->biXPelsPerMeter = DEFAULT_OR_IGNORE;
    header->biYPelsPerMeter = DEFAULT_OR_IGNORE;
    header->biClrImportant = DEFAULT_OR_IGNORE;
    header->biClrUsed = DEFAULT_OR_IGNORE;
    return header;
}

uint32_t get_padding(uint64_t width) {
    return (4 - (width * SIZEOF_PIXEL) % 4) % 4;
}

struct bmp_header* extract_from_image(struct image const* img) {
    struct bmp_header* header = get_default_header();
    if (!header) return NULL;
    header->bfileSize = (get_image_width(img) * SIZEOF_PIXEL + get_padding(get_image_width(img))) * get_image_height(img) + sizeof(struct bmp_header);
    header->bOffBits = sizeof(struct bmp_header);
    header->biWidth = get_image_width(img);
    header->biHeight = get_image_height(img);
    header->biSizeImage = get_image_height(img) * get_image_width(img) * SIZEOF_PIXEL;
    header->biBitCount = SIZEOF_PIXEL * 8;
    return header;
}

struct bmp_header_with_status write_bmp_header(struct image const* source, FILE* out) {
    struct bmp_header* header = extract_from_image(source);
    if (!header) {
            free(header);
            return invalid_write_header(WRITE_INVALID_SOURCE_IMAGE);
    }

    if (!fwrite(header, sizeof(struct bmp_header), 1, out)){
        free(header);
        return invalid_write_header(WRITE_INVALID_HEADER);
    }
    return some_header(header);
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header_with_status header = write_bmp_header(img, out);
    if (header.status.write_status != WRITE_OK) {
        free_bmp_header_with_status(header);
        return header.status.write_status;
    }
    uint32_t padding = get_padding(get_image_width(img));

    size_t bytes_written = 0;
    for (size_t i = 0; i < get_image_height(img); ++i) {
        bytes_written = fwrite(get_pixel_at(get_image_data(img), i * get_image_width(img)), SIZEOF_PIXEL, get_image_width(img), out);
        if (bytes_written != get_image_width(img)) {
            free(header.header);
            return WRITE_INVALID_BITS;
        }
        if (fseek(out, padding, SEEK_CUR)) {
            free(header.header);
            return WRITE_ERROR;
        }
    }

    free(header.header);

    return WRITE_OK;
 }

struct bmp_header_with_status read_bmp_header(FILE* in) {
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    if (!header) return invalid_read_header(READ_ERROR);

    if(!fread(header, sizeof(struct bmp_header), 1, in)) {
        return invalid_read_header(READ_INVALID_HEADER);
    }
    if (header->bfType != BMP_TYPE_ID) return invalid_read_header(READ_INVALID_SIGNATURE);
    return some_header(header);
}

struct bmp_data_with_status read_bmp_data(FILE* in, struct bmp_header* header) {
    struct pixel* data = init_pixel_data(header->biHeight * header->biWidth);

    if (!data || fseek(in, header->bOffBits, SEEK_SET)) {
        return invalid_read_data(READ_ERROR);
    }

    uint32_t padding = get_padding(header->biWidth);

    size_t bytes_read = 0;
    for (size_t i = 0; i < header->biHeight; ++i) {
        bytes_read = fread(get_pixel_at(data, i * header->biWidth), SIZEOF_PIXEL, header->biWidth, in);
        if (bytes_read != header->biWidth) {
            free(data);
            return invalid_read_data(READ_INVALID_BITS);
        }
        if (fseek(in, padding, SEEK_CUR)) {
            return invalid_read_data(READ_ERROR);
        }
    }
    return some_data(data);
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header_with_status header = read_bmp_header(in);
    if (header.status.read_status != READ_OK) {
        free_bmp_header_with_status(header);
        return (enum read_status) header.status.read_status;
    }

    set_image_width(img, header.header->biWidth);
    set_image_height(img, header.header->biHeight);

    struct bmp_data_with_status data = read_bmp_data(in, header.header);

    if (data.status.read_status != READ_OK) {
        free_bmp_data_with_status(data);
        free_bmp_header_with_status(header);
        return (enum read_status) data.status.read_status;
    }

    set_image_data(img, data.data);

    free_bmp_header_with_status(header);

    return READ_OK;
}


