#include <stdint.h>

#ifndef IMAGE_MODULE
#define IMAGE_MODULE

struct image;

struct pixel;

extern const size_t SIZEOF_PIXEL;

extern const size_t SIZEOF_IMAGE;

struct pixel* get_pixel_at(struct pixel* data, size_t index);

void set_image_pixel_from(struct image* dest, size_t to, const struct image* src, size_t from);

uint64_t get_image_height(const struct image* img);

uint64_t get_image_width(const struct image* img);

struct pixel* get_image_data(const struct image* img);

void set_image_data(struct image* img, struct pixel* val);

void set_image_height(struct image* img, uint64_t val);

void set_image_width(struct image* img, uint64_t val);

struct pixel* init_pixel_data(size_t size);

struct image* init_image(void);

struct image* construct_image(uint64_t width, uint64_t height);

struct image* copy_image(const struct image* img);

void deinit_image(struct image** img);

void free_pixel_data(struct pixel** data);

void print_pixel(struct pixel * p);

#endif //IMAGE_MODULE
