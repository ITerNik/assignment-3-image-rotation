#include "image.h"

#ifndef TRANSFORM_MODULE
#define TRANSFORM_MODULE

struct image* rotation(const struct image* img, int64_t angle);

#endif // TRANSFORM_MODULE
