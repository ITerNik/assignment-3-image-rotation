#include "image.h"

#ifndef BMP_MODULE
#define BMP_MODULE

struct bmp_header;

extern const char* const READ_STATUS_TEXT_REPR[];

extern const char* const WRITE_STATUS_TEXT_REPR[];

enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR
};

enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_BITS,
    WRITE_INVALID_SOURCE_IMAGE
};

enum write_status to_bmp( FILE* out, struct image const* img);
enum read_status from_bmp( FILE* in, struct image* img);



#endif // BMP_MODULE


