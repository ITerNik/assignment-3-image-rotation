#ifndef IO_MODULE
#define IO_MODULE

#define ERROR_FILE_CLOSE 0


enum file_status {
    FILE_OK = 1,
    ERROR_OPEN_INPUT_FILE,
    ERROR_OPEN_OUTPUT_FILE
};

enum format_status {
    FORMAT_OK = 0,
    INCORRECT_FORMAT,
    INCORRECT_ANGLE_TEXT,
    ANGLE_NOT_AVAILABLE,
};

struct pipe_file;

extern const char* const FORMAT_ERROR_TEXT_REPR[];

extern const char* const FILE_ERROR_TEXT_REPR[];

enum format_status check_format(char* angle, int32_t argc);

void free_pipe_file(struct pipe_file** pipe);

struct pipe_file* open_pipe_file(char* in_name, char* out_name);

enum file_status get_pipe_file_status(const struct pipe_file* pf);

FILE* get_pipe_file_in(const struct pipe_file* pf);

FILE* get_pipe_file_out(const struct pipe_file* pf);
#endif // IO_MODULE
